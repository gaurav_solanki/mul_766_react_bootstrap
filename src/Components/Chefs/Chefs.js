import React from 'react';
import {Container,Row,Col,Card,CardDeck,Pagination,Table} from "react-bootstrap";
import { IMG_CHEF1, IMG_CHEF2, IMG_CHEF3 } from "../../const/imgConst";

function Chefs() {
    return (
        <div>
            <section id="chefs" class="chefs">
                <Container>
                    <Row>
                        <Col sm={12}>
                            <div class="section-title">
                                <h2>Our Proffesional<span> Chefs</span></h2>
                                <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
                            </div>
                            {/* Table Component */}
                            <Table bordered hover variant="dark" responsive className="mb-5">
                                <thead>
                                    <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Speciality</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Walter </td>
                                        <td>White</td>
                                        <td>Italian</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Sarah </td>
                                        <td>Jhonson</td>
                                        <td>continental</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>William</td>
                                        <td>Anderson</td>
                                        <td>Chinese</td>
                                    </tr>
                                </tbody>
                            </Table>
                            {/* Card and CardDeck Component */}
                            <CardDeck>
                                <Card>
                                    <Card.Img variant="top" src={IMG_CHEF1} />
                                    <Card.Body>
                                    <Card.Title>
                                        <h4>Walter White</h4>
                                    </Card.Title>
                                    <Card.Text>
                                        <p>
                                        Ulamco laboris nisi ut aliquip
                                        ex ea commodo consequat. Et
                                        consectetur ducimus vero placeat
                                        </p>
                                    </Card.Text>
                                    </Card.Body>
                                </Card>
                                <Card>
                                    <Card.Img variant="top" src={IMG_CHEF2} />
                                    <Card.Body>
                                    <Card.Title>
                                        <h4>Sarah Jhonson</h4>
                                    </Card.Title>
                                    <Card.Text>
                                        <p>
                                        Dolorem est fugiat occaecati
                                        voluptate velit esse. Dicta
                                        veritatis dolor quod et vel dire
                                        leno para dest
                                        </p>
                                    </Card.Text>
                                    </Card.Body>
                                </Card>
                                <Card>
                                <Card.Img variant="top" src={IMG_CHEF3} />
                                <Card.Body>
                                <Card.Title>
                                    <h4>William Anderson</h4>
                                </Card.Title>
                                <Card.Text>
                                    <p>
                                    Molestiae officiis omnis illo
                                    asperiores. Aut doloribus vitae
                                    sunt debitis quo vel nam quis
                                    </p>
                                </Card.Text>
                                </Card.Body>
                            </Card>
                            </CardDeck>
                            {/* Pagination Component */}
                            <div className="text-center mt-5">
                                <Pagination className="justify-content-center">
                                    <Pagination.First />
                                    <Pagination.Item active>{1}</Pagination.Item>
                                    <Pagination.Item>{2}</Pagination.Item>
                                    <Pagination.Item >{3}</Pagination.Item>
                                    <Pagination.Item>{4}</Pagination.Item>
                                    <Pagination.Last />
                                </Pagination>                
                            </div>    
                        </Col>
                     </Row>
                </Container>
            </section>            
        </div>
    )
}

export default Chefs
