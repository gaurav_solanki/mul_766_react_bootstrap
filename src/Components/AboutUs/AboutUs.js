import React,{useState} from 'react';
import {Container,Row,Col,Breadcrumb,ButtonGroup,Button,Jumbotron,ListGroup,Tab,Modal,Tooltip,OverlayTrigger} from "react-bootstrap";

function AboutUs() {
    const [modalshow, setShow] = useState(false);

    const handleModalClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const renderTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
          Simple tooltip
        </Tooltip>
    );    

    return (
        <div>
            <section id="about" class="why-us">
                <Container>
                    <Row>
                        <Col lg={12}>
                            {/* Breadcrumb Component */}
                            <Breadcrumb>
                                <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                                <Breadcrumb.Item active>About Us</Breadcrumb.Item>
                            </Breadcrumb>                          
                            <div class="section-title">
                                <h2>Why choose <span>Our Restaurant</span></h2>
                                <div className="text-center mt-3" > 
                                    {/* ButtonGroup Component */}
                                    <ButtonGroup aria-label="Basic example">
                                        <Button variant="secondary">Veg</Button>
                                        <Button variant="secondary">Non-Veg</Button>
                                        <Button variant="secondary">Vegan</Button>
                                    </ButtonGroup>                
                                </div>                               
                                <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
                            </div>
                        </Col>
                        <Col lg={4}>
                            <div className="box">
                                <span>01</span>
                                <h4>Lorem Ipsum</h4>
                                <p> Ulamco laboris nisi ut aliquip ex ea commodo consequat. Et consectetur ducimus vero placeat</p>
                            </div>
                        </Col>
                        <Col lg={4} className="mt-4 mt-lg-0">
                            <div className="box">
                                <span>02</span>
                                <h4>Repellat Nihil</h4>
                                <p>Dolorem est fugiat occaecati voluptate velit esse. Dicta veritatis dolor quod et vel dire leno para dest</p>
                            </div>
                        </Col>
                        <Col lg={4} className="mt-4 mt-lg-0">
                            <div className="box">
                                <span>03</span>
                                <h4> Ad ad velit qui</h4>
                                <p>Molestiae officiis omnis illo asperiores. Aut doloribus vitae sunt debitis quo vel nam quis</p>
                            </div>
                         </Col>
                         <Col lg={12}>
                            {/* Jumbotron ,Overlat,tooltip,Modal Component */}
                            <Jumbotron className="mt-5">
                                <h1>Hello, world!</h1>
                                <p className="mb-2">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                                <div>
                                    <Button variant="secondary" className="mt-3" onClick={handleShow}>Lauch Modal</Button>
                                    <OverlayTrigger placement="right" delay={{ show: 250, hide: 400 }} overlay={renderTooltip}>
                                        <Button variant="secondary" className="mt-3 ml-md-2">Hover me to see</Button>
                                    </OverlayTrigger>
                                </div>
                            </Jumbotron>       
                            {/* tabs and ListGroup Component */}    
                            <Tab.Container id="list-group-tabs-example" defaultActiveKey="#link1">
                                <Row>
                                    <Col sm={4} className="mb-4">
                                        <ListGroup>
                                            <ListGroup.Item action href="#link1">Vegetarian</ListGroup.Item>
                                            <ListGroup.Item action href="#link2">Non-Vegetarian</ListGroup.Item>
                                        </ListGroup>
                                    </Col>
                                    <Col sm={8}>
                                        <Tab.Content>
                                            <Tab.Pane eventKey="#link1">
                                                <p>Why is my verse so barren of new pride, So far from variation or quick change? Why with the time do I not glance aside To new-found methods, and to compounds strange? Why write I still all one, ever the same, And keep invention in a noted weed, That every word doth almost tell my name, Showing their birth, and where they did proceed? O! know sweet love I always write of you, And you and love are still my argument;</p>
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="#link2">
                                                <p>Was it the proud full sail of his great verse, Bound for the prize of all too precious you, That did my ripe thoughts in my brain inhearse, Making their tomb the womb wherein they grew? Was it his spirit, by spirits taught to write, Above a mortal pitch, that struck me dead? No, neither he, nor his compeers by night Giving him aid, my verse astonished. He, nor that affable familiar ghost Which nightly gulls him with intelligence,</p>
                                            </Tab.Pane>
                                        </Tab.Content>
                                    </Col>
                                </Row>
                            </Tab.Container>                                              
                         </Col>
                    </Row>
                </Container>
            </section>      
            <Modal show={modalshow} onHide={handleModalClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleModalClose}>Close</Button>
                    <Button variant="primary" onClick={handleModalClose}>Save Changes</Button>
                </Modal.Footer>
            </Modal>                  
        </div>
    )
}

export default AboutUs
