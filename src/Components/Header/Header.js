import React , {useState} from 'react';
import {Container,Navbar,Nav,Badge,Alert,Button,Dropdown,DropdownButton,Toast,Image} from "react-bootstrap";
import 'holderjs';

function Header() {
    const [show, setShow] = useState(true);
    const [showToast, setShowToast] = useState(true);

    return (
        <div>
            {/* Navbar,dropdown and badge Component */}
            <Navbar
            collapseOnSelect
            expand="lg"
            variant="primary"
            fixed="top"
            className="navbar"
            >
            <Container>
                <Navbar.Brand href="#hero" className="header-logo">
                Gobble
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Link href="#hero">Home</Nav.Link>
                    <Nav.Link href="#about">About</Nav.Link>
                    <Nav.Link href="#chefs">Chefs</Nav.Link>
                    <Nav.Link href="#contact">Contact</Nav.Link>
                    <Nav.Link href="#">Notification &nbsp;<Badge pill variant="secondary">10</Badge></Nav.Link>
                    <DropdownButton
                        menuAlign="right"
                        title="Jhon Doe"
                        id="dropdown-menu-align-right"
                        variant="secondary"
                        className="ml-lg-2"
                    >
                        <Dropdown.Item eventKey="1">My Account</Dropdown.Item>
                        <Dropdown.Item eventKey="2">Settings</Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item eventKey="4">Logout</Dropdown.Item>
                    </DropdownButton>
                </Nav>
                </Navbar.Collapse>
            </Container>
            </Navbar>

            {/* Alert Component */}
             <Alert show={show} variant="danger" onClose={() => setShow(false)} className="custom_alert">
                <Alert.Heading>Warning !</Alert.Heading>
                <p>Our website will store the cookies </p>
                <div className="d-flex justify-content-end">
                    <Button onClick={() => setShow(false)} variant="primary">
                       accept 
                    </Button>
                </div>                
            </Alert>

            {/* Toast Component */}
            <Toast onClose={() => setShowToast(false)} show={showToast} delay={3000} autohide>
                <Toast.Header>
                    <Image src="holder.js/20x20?bg=35322d" roundedCircle className="mr-2" />
                    <strong className="mr-auto">Bootstrap</strong>
                    <small>11 mins ago</small>
                </Toast.Header>
                <Toast.Body>Hello, world! This is a toast message.</Toast.Body>
            </Toast>            
        </div>
    )
}

export default Header
