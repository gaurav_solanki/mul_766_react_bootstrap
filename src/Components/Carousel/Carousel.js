import React from 'react';
import {Carousel} from "react-bootstrap";

function carousel() {
    return (
        <div>
            {/* Carousel Component */}
            <Carousel className="carousel-container" id="hero">
                <Carousel.Item className="slide1">
                    <div class="carousel-content">
                        <div>
                            <h2><span>Gobble</span> Restaurant</h2>
                            <p>Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item className="slide2">
                    <div class="carousel-content">
                        <div>
                            <h2><span>Lorem</span> Ipsum Dolor</h2>
                            <p> Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item className="slide3">
                <div class="carousel-content">
                    <div>
                        <h2><span> Sequi ea ut</span> et est quaerat</h2>
                        <p>Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                    </div>
                </div>
            </Carousel.Item>
            </Carousel>            
        </div>
    )
}

export default carousel
