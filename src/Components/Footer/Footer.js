import React from 'react';
import {Container} from "react-bootstrap";

function Footer() {
    return (
        <div>
            <footer id="footer">
                <Container>
                    <h3>Gobble</h3>
                    <div class="copyright">&copy; Copyright<strong><span>Gobble</span></strong>. All Rights Reserved</div>
                </Container>
            </footer>            
        </div>
    )
}

export default Footer
