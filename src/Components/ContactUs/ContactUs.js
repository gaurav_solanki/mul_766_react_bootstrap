import React from 'react';
import {Row,Col,Container,Form,Button,InputGroup,ProgressBar,Spinner} from "react-bootstrap";

function ContactUs() {
    return (
      <div>
        <section id="contact" class="contact">
          <Container>
            <div class="section-title">
              <h2><span>Contact</span> Us</h2>
              <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
            </div>
            <Form>
              <Row>
                <Col lg={6}>
                  <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Control
                      size="lg"
                      type="text"
                      placeholder="Your Name"
                      className="custom_field"
                    />
                  </Form.Group>
                </Col>
                <Col lg={6}>
                  {/* Input Group Component */}
                  <InputGroup className="mb-3">
                    <Form.Control
                      size="lg"
                      type="email"
                      placeholder="Your Email"
                      className="custom_field"
                      aria-describedby="basic-addon2"
                    />
                    <InputGroup.Append>
                      <InputGroup.Text id="basic-addon2">@example.com</InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>                  
                </Col>
                <Col lg={12}>
                  <Form.Group controlId="exampleForm.ControlInput3">
                    <Form.Control
                      size="lg"
                      type="text"
                      placeholder="Subject"
                      className="custom_field"
                    />
                  </Form.Group>
                </Col>
                <Col lg={12}>
                  <Form.Group controlId="exampleForm.ControlInput4">
                    <Form.Control
                      size="lg"
                      as="textarea"
                      rows={3}
                      placeholder="Message"
                      className="custom_field"
                    />
                  </Form.Group>
                </Col>
                <Col lg={12} className="text-center">
                  {/* Progressbar and spinner Component */}
                  <ProgressBar striped animated variant="secondary" now={40} className="mb-4" />
                  <Form.Group controlId="exampleForm.ControlInput4">
                    <Button size="lg" variant="secondary">
                      Send Message
                      <Spinner animation="border" className="ml-2" />                   
                    </Button>
                  </Form.Group>
                </Col>
              </Row>
            </Form>
          </Container>
        </section>            
      </div>
    )
}

export default ContactUs
