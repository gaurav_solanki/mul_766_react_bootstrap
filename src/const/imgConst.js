const IMG_SLIDER1 = require('../images/slide-1.jpg').default;
const IMG_SLIDER2 = require('../images/slide-2.jpg').default;
const IMG_SLIDER3 = require('../images/slide-3.jpg').default;
const IMG_CHEF1 = require('../images/chefs-1.jpg').default;
const IMG_CHEF2 = require('../images/chefs-2.jpg').default;
const IMG_CHEF3 = require('../images/chefs-3.jpg').default;


export{
  IMG_SLIDER1,
  IMG_SLIDER2,
  IMG_SLIDER3,
  IMG_CHEF1,
  IMG_CHEF2,
  IMG_CHEF3,
};
