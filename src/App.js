import React, { Component } from 'react';
import Header from './Components/Header/Header';
import Carousel from './Components/Carousel/Carousel';
import AboutUs from './Components/AboutUs/AboutUs';
import Chefs from './Components/Chefs/Chefs';
import ContactUs from './Components/ContactUs/ContactUs';
import Footer from './Components/Footer/Footer';
import "./App.css";
 
export default class App extends Component {

  render() {
    return (
      <div> 
        <Header />      
        <Carousel />
        <AboutUs />
        <Chefs />
        <ContactUs />
        <Footer />
      </div>
    );
  }
}
